# WINmemory - README #
---

### Overview ###

The **WINmemory** application is a small memory game for Windows.

### Screenshots ###

![WINmemory - Main window](development/readme/winmemory1.png "WINmemory - Main window")

### Setup ###

* Start the **WINmemory.exe** executable with a doubleclick.
* Select the different cards with the mouse to show the images below.
* Try to match the images to remove them from the game.
* Please also check the **WINmemory** user manual!

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **WINmemory** application is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
